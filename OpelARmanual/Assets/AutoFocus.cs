﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AutoFocus : MonoBehaviour {
    // Start is called before the first frame update
    void Awake () {
        CameraDevice.Instance.Deinit ();
        CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        CameraDevice.Instance.Init ();
    }
}