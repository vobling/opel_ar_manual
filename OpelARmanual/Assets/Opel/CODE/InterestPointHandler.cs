﻿using UnityEngine;

public class InterestPointHandler : Singleton<InterestPointHandler>
{
    [SerializeField] InterestPoint[] interestPoints;

    public void ToggleInterestPoints(InterestPoint point)
    {
        if (point.pointIsOpen)
        {
            point.Toggle(false);
        }
        else
        {
            foreach (InterestPoint p in interestPoints)
            {
                if (point != p)
                {
                    p.Toggle(false);
                }
                else p.Toggle(true);
            }
        }
    }

    public void CloseAll()
    {
        foreach (InterestPoint point in interestPoints)
        {
            point.Toggle(false);
        }
    }
}
