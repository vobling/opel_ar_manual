﻿using UnityEngine;
using UnityEngine.Events;

public class InterestPoint : MonoBehaviour
{
    public bool pointIsOpen;
    public Animator anim;
    [SerializeField] string animParamName;
    public bool implemented = true;
    public PowerUI.UIRenderer renderer;

    public UnityEvent onNotImplementedSelection, onSelection, onDeselection;

    public Procedure[] procedures;

    private void Start()
    {
        onDeselection.Invoke();
        foreach (var p in procedures)
        {
            p.DeselectThis();
        }
    }

    public void SetProcedure(int procedure)
    {
        for (int i = 0; i < procedures.Length; i++)
        {
            if (i != procedure)
            {
                procedures[i].DeselectThis();
            }
            else procedures[i].SelectThis();
        }

        //if (renderer.HtmlFile != procedures[procedure].htmlFile)
            renderer.ResetUI(procedures[procedure].htmlFile);
    }

    public void Toggle(bool open)
    {
        pointIsOpen = open;
        anim.SetBool(animParamName, open);

        if (open) onSelection.Invoke();
        else
        {
            onDeselection.Invoke();
            foreach (var p in procedures)
            {
                p.OnDeselection.Invoke();
            }
        }
    }

    [ContextMenu("Close this point")]
    public void CloseThisPoint()
    {
        if (implemented)
        {
            InterestPointHandler.Instance.ToggleInterestPoints(this);
        }
        else onNotImplementedSelection.Invoke();

    }

    private void OnMouseDown()
    {
        CloseThisPoint();
    }
}
