﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollAllWayup : MonoBehaviour
{
    public ScrollRect scroller;

    public void Scroll()
    {
        scroller.verticalNormalizedPosition = 1;
    }
}
