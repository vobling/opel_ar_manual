﻿using UnityEngine;
using UnityEngine.Events;

public enum ViewingType { AR, Flat, LAI }

public class ModeManager : Singleton<ModeManager>
{

    public ViewingType viewingType;

    public HighlightParent[] parents;
    [SerializeField]ManualOverlayPanel panel;

    public UnityEvent OnArSelection, OnFlatSelection, OnLaiSelection;

    [ContextMenu("Find all highlightparents")]
    public void FindAllParents()
    {
        parents = FindObjectsOfType<HighlightParent>();
    }

    private void Start()
    {
        SetAR();
    }

    [ContextMenu("SetAr")]
    public void SetAR()
    {
        SetMode(ViewingType.AR);
        OnArSelection.Invoke();
    }

    [ContextMenu("SetFlat")]
    public void SetFlat()
    {
        SetMode(ViewingType.Flat);
        OnFlatSelection.Invoke();
    }

    [ContextMenu("Set LAI")]
    public void SetLAI()
    {
        SetMode(ViewingType.LAI);
        OnLaiSelection.Invoke();
    }

    public void SetMode(ViewingType vt)
    {
        viewingType = vt;

        Debug.Log("Viewtype is now: " + vt.ToString());

        InterestPointHandler.Instance.CloseAll();
        panel.CloseMenu();

        foreach (var h in parents)
        {
            if (vt == ViewingType.AR)
            {
                h.SetMask();
            }
            else if (vt == ViewingType.Flat)
            {
                h.SetFlatMode();
            }
        }
    }
}
