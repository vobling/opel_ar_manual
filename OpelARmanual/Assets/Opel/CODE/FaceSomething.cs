﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceSomething : MonoBehaviour
{
    [SerializeField] bool onlyY = true;
    [SerializeField] bool negateLookDirection = true;
    [SerializeField] public Transform lookAtObject;

    private void Start()
    {
        if (lookAtObject == null)
        {
            lookAtObject = Camera.main.transform;
        }
    }

    private void Update()
    {
        transform.rotation = lookAtObject.rotation;
        transform.Rotate(Vector3.up*180);
    }
}
