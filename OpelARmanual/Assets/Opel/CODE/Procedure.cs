﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Procedure
{
    public string procedureID;
    public TextAsset htmlFile;
    public UnityEvent OnSelection, OnDeselection;
    
    
    public void SelectThis()
    {
        OnSelection.Invoke();
    }

    

    public void DeselectThis()
    {
        OnDeselection.Invoke();
    }
}
