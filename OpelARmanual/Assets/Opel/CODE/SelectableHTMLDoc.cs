﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableHTMLDoc : MonoBehaviour
{
    [SerializeField]PowerUI.UIRenderer renderer;



    private void OnMouseDown()
    {
        ManualOverlayPanel.Instance.OpenMenu(renderer.HtmlFile);
    }
}
