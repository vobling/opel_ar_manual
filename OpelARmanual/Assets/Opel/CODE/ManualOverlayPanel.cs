﻿using UnityEngine;

public class ManualOverlayPanel : Singleton<ManualOverlayPanel>
{
    [SerializeField] ToggleAnimation animationToggle;
    [SerializeField] PowerUI.HtmlUIPanel panel;
    [SerializeField] ScrollAllWayup scroller;
    
    public void OpenMenu(TextAsset htmlFile)
    {
        panel.HtmlFile = htmlFile;
        scroller.Scroll();
        animationToggle.Toggle(true);
    }

    public void CloseMenu()
    {
        animationToggle.Toggle(false);
    }
}
