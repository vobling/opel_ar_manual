﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatModeToggle : MonoBehaviour
{
    [SerializeField]GameObject flatmodeThings, ARThings;

    private void Start()
    {
        SetARMode();
    }

    [ContextMenu("Set To ar")]
    public void SetARMode()
    {
        flatmodeThings.SetActive(false);
        ARThings.SetActive(true);
    }

    [ContextMenu("Set To Flat")]
    public void SetFlatMode()
    {
        flatmodeThings.SetActive(true);
        ARThings.SetActive(false);

    }
}
