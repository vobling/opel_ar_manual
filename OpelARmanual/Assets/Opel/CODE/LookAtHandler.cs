﻿using UnityEngine;

public class LookAtHandler : Singleton<LookAtHandler>
{
    public FaceSomething[] lookAtObjects;

    public Transform arCamera, flatCamera;

    [ContextMenu("Find all lookats")]
    public void FindAllLookAts()
    {
        lookAtObjects = FindObjectsOfType<FaceSomething>();
    }

    public void SetARCamera()
    {
        foreach (var lookat in lookAtObjects)
        {
            lookat.lookAtObject = arCamera;
        }
    }

    public void SetFlatCamera()
    {
        foreach (var lookat in lookAtObjects)
        {
            lookat.lookAtObject = flatCamera;
        }
    }
}
