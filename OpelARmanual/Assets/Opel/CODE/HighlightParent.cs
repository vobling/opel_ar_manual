﻿using System.Collections.Generic;
using UnityEngine;

public class HighlightParent : MonoBehaviour
{
    [SerializeField] List<Highlightable> highlightable;

    [SerializeField] Material highlightMat, maskMat;

    [SerializeField] List<GameObject> children;

    [ContextMenu("Apply and assign highlighables")]
    public void AssignAll()
    {
        highlightable = new List<Highlightable>();
        children = new List<GameObject>();


        for (int i = 0; i < transform.childCount; i++)
        {
            children.Add(transform.GetChild(i).gameObject);
        }

        foreach (var child in children)
        {
            if (child.GetComponent<Highlightable>())
            {
                DestroyImmediate(child.GetComponent<Highlightable>());
            }
            Highlightable h = child.AddComponent<Highlightable>();
            h.Setup(highlightMat, maskMat);
            highlightable.Add(h);
        }

        ModeManager.Instance.FindAllParents();
    }

    private void Start()
    {
        if (ModeManager.Instance.viewingType == ViewingType.AR)
        {
            SetMask();
        }
        else SetFlatMode();
    }

    public void SetHighLighted()
    {
        foreach (var h in highlightable)
        {
            h.SetHighLighted();
        }
    }

    public void CloseHighlight()
    {
        if (ModeManager.Instance.viewingType == ViewingType.AR)
        {
            SetMask();
        }
        else SetFlatMode();
    }

    public void SetFlatMode()
    {
        foreach (var h in highlightable)
        {
            h.SetFlatMode();
        }
    }

    public void SetMask()
    {
        foreach (var h in highlightable)
        {
            h.SetMask();
        }
    }
}
