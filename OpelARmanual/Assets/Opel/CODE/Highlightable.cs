﻿using UnityEditor;
using UnityEngine;

public class Highlightable : MonoBehaviour
{
    public Material highLightMaterial, maskMaterial, flatModeMaterial;

    public Renderer renderer;

    public  void Setup(Material highlight, Material mask)
    {
        renderer = GetComponent<Renderer>();
        highLightMaterial = highlight;
        maskMaterial = mask;
        flatModeMaterial = renderer.sharedMaterial;
    }

    public void SetHighLighted()
    {
        SetMaterial(highLightMaterial);
    }

    public void SetFlatMode()
    {
        SetMaterial(flatModeMaterial);
    }

    public void SetMask()
    {
        SetMaterial(maskMaterial);
    }

    void SetMaterial(Material mat)
    {
        renderer.material = mat;
    }
}