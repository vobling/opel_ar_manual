﻿using UnityEngine;

public class ToggleAnimation : MonoBehaviour
{
    public Animator anim;
    public string parameterName;

    public void Toggle(bool open)
    {
        anim.SetBool(parameterName, open);
    }
    public void Toggle()
    {
        anim.SetBool(parameterName, !anim.GetBool(parameterName));
    }

    public void Toggle(int intSelection)
    {
        anim.SetInteger(parameterName,intSelection);
    }
}
