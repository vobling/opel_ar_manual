﻿using System.Collections;
using System.Collections.Generic;
using PowerUI;
using UnityEngine;
using UnityEngine.UI;

public class IndicatorScreen : Window {

    [SerializeField]
    private Animator buttonsAnim, pageAnim;
    [SerializeField]
    string animParam;
    public Indicator indicatorData;
    public Image pageImage;
    //public RawImage pageImage;
    public HtmlUIPanel page;

    public static IndicatorScreen Instance {
        get;
        private set;
    }

    private void Awake () {
        Instance = this;
        //anim = GetComponent<Animator> ();
        Evaluate ();
    }

    public void SetIndicatorData (Indicator data) {
        indicatorData = data;
    }

    protected override void Evaluate () {
        //pageImage.sprite = indicatorData?.manualPage;
        //StartCoroutine (UpdateHTML ());
        buttonsAnim.SetBool (animParam, !isOpen);
        pageAnim.SetBool (animParam, isOpen);
        pageImage.sprite = indicatorData?.manualPage;
        // IEnumerator UpdateHTML () {
        //     page.enabled = false;
        //     page.HtmlFile = indicatorData.htmlFile;
        //     yield return new WaitForFixedUpdate ();
        //     page.enabled = true;
        // }
    }

}