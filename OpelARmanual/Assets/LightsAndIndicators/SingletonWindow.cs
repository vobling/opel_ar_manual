﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonWindow : Singleton<SingletonWindow> {
    /// <summary>
    /// Status of the window
    /// </summary>
    protected bool isOpen = false;

    /// <summary>
    /// Basic windowaction returning nothing and takes no params
    /// </summary>
    public event Action _OnOpen;
    public event Action _OnClose;

    public virtual void Open () {
        isOpen = true;
        _OnOpen?.Invoke ();
        Evaluate ();
    }

    public virtual void Close () {
        isOpen = false;
        _OnClose?.Invoke ();
        Evaluate ();
    }

    public void SetOpen (bool value) {
        if (value) {
            Open ();
        } else {
            Close ();
        }
    }

    public void Toggle () {
        if (isOpen) {
            Close ();
        } else {
            Open ();
        }
    }

    /// <summary>
    /// Used to implement what is supposed to happen when opening and closing
    /// </summary>
    protected abstract void Evaluate ();
}