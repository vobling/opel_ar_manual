﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IndicatorButton : MonoBehaviour {
    public Image icon;
    public Indicator indicatorAsset;

    private void Awake () {
        name = indicatorAsset.name;
        icon.sprite = indicatorAsset.icon;

        GetComponent<Button> ().onClick.AddListener (OnClickedListener);
    }

    private void OnClickedListener () {
        IndicatorScreen.Instance.SetIndicatorData (indicatorAsset);
        IndicatorScreen.Instance.Open ();
    }

}