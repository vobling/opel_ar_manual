﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Indicator : ScriptableObject {
    public string m_completelyDifferentName;
    public Sprite icon;
    public Sprite manualPage;
    public TextAsset htmlFile;
}