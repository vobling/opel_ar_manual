<!DOCTYPE html><html>
   <head xmlns:xs="http://www.w3.org/2001/XMLSchema">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Stop-start system</title>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="css/style.css"><script src="js/custom.js" type="text/javascript"></script></head>
   <body>
      <div id="ftnote-overlay"></div>
      <div class="cell-03002">
         <div class="app-header">
            <h2 id="i03002-01">Stop-start system</h2>
         </div>
         <p>The stop-start system helps to save fuel and to reduce the exhaust emissions. When
            conditions allow, it switches off the engine as soon as the vehicle is at a low speed
            or at a standstill, <span class="nb-text">e.g.</span> at a traffic light or in a traffic jam.
         </p>
         <div class="oiesub">
            <h3 id="i03002-02">Activation</h3>
            <p>The stop-start system is available as soon as the engine is started, the vehicle starts-off
               and the conditions as stated below in this section are fulfilled.
            </p>
         </div>
         <div class="oiesub">
            <h3 id="i03002-03">Deactivation</h3>
            <div class="figure column-wide">
               <div class="graphic"><img src="content_img/img84b58938d258e1e3c0a8014728086aa9_1_--_--_PNG_MQ.png" alt="38801"></div>
            </div>
            <p>Deactivate the stop-start system manually by pressing <span class="symbol Font_2018">Ò</span>. The deactivation is indicated when the LED in the button illuminates.
            </p>
         </div>
         <div class="oiesub">
            <h3 id="i03002-04">Autostop</h3>
            <div class="block">
               <h5>Vehicles with manual transmission</h5>
               <p>An Autostop can be activated at a standstill.</p>
               <p>Activate an Autostop as follows:</p>
               <ul>
                  <li><span>
                        <p>Depress the clutch pedal.</p></span></li>
                  <li><span>
                        <p>Set the selector lever to neutral.</p></span></li>
                  <li><span>
                        <p>Release the clutch pedal.</p></span></li>
               </ul>
               <p>The engine will be switched off while the ignition stays on.</p>
            </div>
            <div class="block">
               <h5>Vehicles with automatic transmission</h5>
               <p>If the vehicle is at a standstill with depressed brake pedal, Autostop is activated
                  automatically.
               </p>
               <p>The engine will be switched off while the ignition stays on.</p>
               <p>The stop-start system will be disabled on inclines of 12% or more.</p>
            </div>
            <div class="oiesubsub">
               <h4 id="i03002-07">Indication</h4>
               <div class="figure column-wide">
                  <div class="graphic"><img src="content_img/imge315ef6b9028c7f2c0a801ea200cf7a2_2_--_--_PNG_MQ.png" alt="35632"></div>
               </div>
               <p>An Autostop is indicated by control indicator <span class="symbol Infotainment_2013">D</span>.
               </p>
               <p>During an Autostop, the heating and brake performance will be maintained. </p>
            </div>
            <div class="oiesubsub">
               <h4 id="i03002-08">Conditions for an Autostop</h4>
               <p>The stop-start system checks if each of the following conditions is fulfilled. </p>
               <ul>
                  <li><span>
                        <p>The stop-start system is not manually deactivated. </p></span></li>
                  <li><span>
                        <p>The driver's door is closed or the driver's seat belt is fastened.</p></span></li>
                  <li><span>
                        <p>The vehicle battery is sufficiently charged and in good condition.</p></span></li>
                  <li><span>
                        <p>The engine is warmed up.</p></span></li>
                  <li><span>
                        <p>The engine coolant temperature is not too high.</p></span></li>
                  <li><span>
                        <p>The engine exhaust temperature is not too high, e.g. after driving with high engine
                           load.
                        </p></span></li>
                  <li><span>
                        <p>The ambient temperature is not too low or too high.</p></span></li>
                  <li><span>
                        <p>The climate control system allows an Autostop. </p></span></li>
                  <li><span>
                        <p>The brake vacuum is sufficient.</p></span></li>
                  <li><span>
                        <p>The self-cleaning function of the exhaust filter is not active.</p></span></li>
                  <li><span>
                        <p>The vehicle was driven at least at walking speed since the last Autostop.</p></span></li>
               </ul>
               <p>Otherwise an Autostop will be inhibited.</p>
               <div class="wcnd notice">
                  <div class="header">
                     <p><span class="title">Note</span></p>
                  </div>
                  <div class="body">
                     <p>The Autostop may be inhibited for several hours after a battery replacement or reconnection.</p>
                  </div>
               </div>
               <p>Certain settings of the climate control system may inhibit an Autostop.</p>
               <p>Climate control <span class="cell-link"><a href="0282.html">Air conditioning system</a></span>.
               </p>
               <p>Immediately after higher speed driving an Autostop may be inhibited.</p>
               <p>New vehicle running-in <span class="cell-link"><a href="0296.html">New vehicle running-in</a></span>.
               </p>
            </div>
            <div class="oiesubsub">
               <h4 id="i03002-09">Vehicle battery discharge protection</h4>
               <p>To ensure reliable engine restarts, several vehicle battery discharge protection features
                  are implemented as part of the stop-start system.
               </p>
            </div>
            <div class="oiesubsub">
               <h4 id="i03002-10">Power saving measures</h4>
               <p>During an Autostop, several electrical features such as auxiliary electric heater
                  or rear window heating are disabled or switched to a power saving mode. The fan speed
                  of the climate control system is reduced to save power.
               </p>
            </div>
         </div>
         <div class="oiesub">
            <h3 id="i03002-11">Restart of the engine by the driver</h3>
            <div class="oiesubsub">
               <h4 id="i03002-12">Vehicles with manual transmission</h4>
               <p>Depress the clutch pedal without depressing the brake pedal to restart the engine.</p>
            </div>
            <div class="oiesubsub">
               <h4 id="i03002-13">Vehicles with automatic transmission</h4>
               <p>The engine is restarted if </p>
               <ul>
                  <li><span>
                        <p>the brake pedal is released while the selector lever is in position <span class="interior">D</span> or <span class="interior">M</span></p></span></li>
                  <li><span>
                        <p>or the brake pedal is released or the selector lever is in position <span class="interior">N</span> when the selector lever is moved to position <span class="interior">D</span> or <span class="interior">M</span></p></span></li>
                  <li><span>
                        <p>or the selector lever is moved to position <span class="interior">R</span>.
                        </p></span></li>
               </ul>
            </div>
         </div>
         <div class="oiesub">
            <h3 id="i03002-14">Restart of the engine by the stop-start system</h3>
            <p>The selector lever must be in neutral to enable an automatic restart.</p>
            <p>If one of the following conditions occurs during an Autostop, the engine will be restarted
               automatically by the stop-start system:
            </p>
            <ul>
               <li><span>
                     <p>The stop-start system is manually deactivated.</p></span></li>
               <li><span>
                     <p>The driver's seat belt is unfastened and the driver's door is opened. </p></span></li>
               <li><span>
                     <p>The engine temperature is too low.</p></span></li>
               <li><span>
                     <p>The charging level of the vehicle battery is below a defined level. </p></span></li>
               <li><span>
                     <p>The brake vacuum is not sufficient.</p></span></li>
               <li><span>
                     <p>The vehicle is driven at least at walking speed.</p></span></li>
               <li><span>
                     <p>The climate control system requests an engine start.</p></span></li>
               <li><span>
                     <p>The air conditioning is manually switched on.</p></span></li>
            </ul>
            <p>If an electrical accessory, e.g. a portable CD player, is connected to the power outlet,
               a brief power drop during the restart might be noticeable.
            </p>
         </div>
      </div>
   </body>
</html>