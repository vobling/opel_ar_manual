/* JavaScript for GM mobile owner's information content.
 * Provides footnotes handling and interaction.
 * Copyright 2014 CBG, General Motors 

 window.onload = function() {

	//Array of all ftnote elements
	var ftnotes = document.getElementsByClassName("ftnote");

	//Array of all ftnote content elements
	var ftnotesContent = document.getElementsByClassName("ftnote-content");

	var ftnoteOverlay = document.getElementById('ftnote-overlay');
	ftnoteOverlay.addEventListener('click', closeFootnote, false);

	//Add event listener to them
	for(var i = 0; i < ftnotes.length; i++){
		ftnotes[i].addEventListener('click', openFootnote, false);
	};

	//FUNCTIONS
	function openFootnote() {
		//Show overlay (background)
		ftnoteOverlay.style.display = 'block';


		//Target footnote ID
		var targetId = event.srcElement.parentNode.getAttribute("data-target");

		//Show target footnote content
		var ftnote = document.getElementById(targetId);
		ftnote.style.display = 'block';

		//Event listener for close function, ftnote content click
		ftnote.addEventListener('click', closeFootnote, false);

		//Change to active ftnote icon
		event.srcElement.setAttribute("src", "static_img/footnote_active.png");
	};

	function closeFootnote() {
		//Hide overlay (background)
		ftnoteOverlay.style.display = 'none';

		for(var i = 0; i < ftnotesContent.length; i++){
			ftnotesContent[i].style.display = 'none';
		};

		//Change to active ftnote icon
		for(var i = 0; i < ftnotes.length; i++){
			ftnotes[i].children[0].setAttribute("src", "static_img/footnote_inactive.png");
		}
	}
}*/